import Vue from 'vue'
import Router from 'vue-router'
import App from "../App";
import Home from "../views/Home";
import CinemaHome from '../views/CinemaHome'
import CinemaHomeContent from "../views/CinemaHomeContent";
import Show from '../views/Show'
import ScreeningRoom from "../components/ScreeningRoom";
import NotFound from '../views/NotFound'
import ComingSoon from '../views/ComingSoon'
import MovieDetails from '../views/MovieDetails'
import PriceList from "../views/PriceList";
import Login from "../views/Login";
import SignUp from "../views/SignUp";
import AboutUs from "../views/AboutUs"
import Contact from "../views/Contact"
import AdminPanel from "../views/admin/AdminPanel";
import CinemaPanel from "../views/admin/CinemaPanel";
import EmployeePanel from "../views/admin/EmployeePanel";
import MoviePanel from "../views/admin/MoviePanel";
import RoomPanel from "../views/admin/RoomPanel";
import ShowPanel from "../views/admin/ShowPanel";
import StaffPanel from "../views/admin/StaffPanel";
import TicketTypePanel from "../views/admin/TicketTypePanel";
import AdminLogin from "../views/admin/AdminLogin";
import ArmchairPanel from "../views/admin/ArmchairPanel";
import EmployeeLogin from "../views/employee/EmployeeLogin";
import EmployeeApp from "../views/employee/EmployeeApp";
import EmployeeReservation from "../views/employee/EmployeeReservation";
import EmployeeConfirmReservation from "../views/employee/EmployeeConfirmReservation";
import EmployeeTickets from "../views/employee/EmployeeTickets";
import EmployeeScreeningRoomReservation from "../components/employee/EmployeeScreeningRoomReservation";
import EmployeeScreeningRoomTickets from "../components/employee/EmployeeScreeningRoomTickets";

import store from '../store/index.js'
import {loginService} from "../services/login.service";
import UserAccount from "../views/UserAccount";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'App',
      component: App,
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'Home',
          component: Home,
          beforeEnter: (to, from, next) => {
            if (!!store.getters['store/isCinemaSet'] && (to.fullPath === '/home' || to.fullPath === '/')) {
              next({name: 'CinemaHome'});
            } else {
              next();
            }
          }
        },
        {
          path: 'cinema',
          name: 'CinemaHome',
          component: CinemaHome,
          beforeEnter: (to, from, next) => {
            if (!!!store.getters['store/isCinemaSet']) {
              next({name: 'Home'});
            } else {
              next();
            }
          },
          children: [
            {
              path: 'main',
              name: 'CinemaHomeContent',
              component: CinemaHomeContent
            },
            {
              path: 'show/:showId',
              name: 'ScreeningRoom',
              component: ScreeningRoom
            },
            {
              path: 'show',
              name: 'Show',
              component: Show
            },
            {
              path: 'coming-soon',
              name: 'ComingSoon',
              component: ComingSoon
            },
            {
              path: 'movie/:movieId',
              name: 'Movie',
              component: MovieDetails,
            },
            {
              path: 'login',
              name: 'Login',
              component: Login,
            },
            {
              path: 'sign-up',
              name: 'SignUp',
              component: SignUp,
            },
            {
              path: 'account/settings',
              name: 'AccountSettings',
              component: UserAccount,
            },
            {
              path: 'about-us',
              name: 'AboutUs',
              component: AboutUs,
            },
            {
              path: 'price-list',
              name: 'PriceList',
              component: PriceList,
            },
            {
              path: 'contact',
              name: 'Contact',
              component: Contact,
            },
          ]
        },
        {
          path: 'admin-login',
          name: 'AdminLogin',
          component: AdminLogin,

        },
        {
          path: 'admin-panel',
          name: 'AdminPanel',
          component: AdminPanel,
          beforeEnter: (to, from, next) => {
            if (localStorage.getItem('user') != null) {
              if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                next();
              } else {
                next({name: 'AdminLogin'});
              }
            } else {
              next({name: 'AdminLogin'});
            }
          },
          children: [
            {
              path: 'cinema-panel',
              name: 'CinemaPanel',
              component: CinemaPanel,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                    next();
                  } else {
                    next({name: 'AdminLogin'});
                  }
                } else {
                  next({name: 'AdminLogin'});
                }
              },
            },
            {
              path: 'employee-panel',
              name: 'EmployeePanel',
              component: EmployeePanel,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                    next();
                  } else {
                    next({name: 'AdminLogin'});
                  }
                } else {
                  next({name: 'AdminLogin'});
                }
              },
            },
            {
              path: 'movie-panel',
              name: 'MoviePanel',
              component: MoviePanel,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                    next();
                  } else {
                    next({name: 'AdminLogin'});
                  }
                } else {
                  next({name: 'AdminLogin'});
                }
              },

            },
            {
              path: 'room-panel',
              name: 'RoomPanel',
              component: RoomPanel,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                    next();
                  } else {
                    next({name: 'AdminLogin'});
                  }
                } else {
                  next({name: 'AdminLogin'});
                }
              },
            },
            {
              path: 'armchair-panel',
              name: 'ArmchairPanel',
              component: ArmchairPanel,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                    next();
                  } else {
                    next({name: 'AdminLogin'});
                  }
                } else {
                  next({name: 'AdminLogin'});
                }
              },
            },
            {
              path: 'show-panel',
              name: 'ShowPanel',
              component: ShowPanel,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                    next();
                  } else {
                    next({name: 'AdminLogin'});
                  }
                } else {
                  next({name: 'AdminLogin'});
                }
              },
            },
            {
              path: 'staff-panel',
              name: 'StaffPanel',
              component: StaffPanel,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                    next();
                  } else {
                    next({name: 'AdminLogin'});
                  }
                } else {
                  next({name: 'AdminLogin'});
                }
              },
            },
            {
              path: 'ticket-type-panel',
              name: 'TicketTypePanel',
              component: TicketTypePanel,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_ADMIN') {
                    next();
                  } else {
                    next({name: 'AdminLogin'});
                  }
                } else {
                  next({name: 'AdminLogin'});
                }
              },
            }
          ]
        },
        {
          path: 'employee-login',
          name: 'EmployeeLogin',
          component: EmployeeLogin,
        },
        {
          path: 'employee-app',
          name: 'EmployeeApp',
          component: EmployeeApp,
          beforeEnter: (to, from, next) => {
            if (localStorage.getItem('user') != null) {
              if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_EMPLOYEE') {
                next();
              } else {
                next({name: 'EmployeeLogin'});
              }
            } else {
              next({name: 'EmployeeLogin'});
            }
          },
          children: [
            {
              path: 'reservation',
              name: 'Reservation',
              component: EmployeeReservation,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_EMPLOYEE') {
                    next();
                  } else {
                    next({name: 'EmployeeLogin'});
                  }
                } else {
                  next({name: 'EmployeeLogin'});
                }
              },
            },
            {
              path: 'screening-room/reservation/:showId',
              name: 'EmployeeScreeningRoomReservation',
              component: EmployeeScreeningRoomReservation,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_EMPLOYEE') {
                    next();
                  } else {
                    next({name: 'EmployeeLogin'});
                  }
                } else {
                  next({name: 'EmployeeLogin'});
                }
              },
            },
            {
              path: 'screening-room/tickets/:showId',
              name: 'EmployeeScreeningRoomTickets',
              component: EmployeeScreeningRoomTickets,
              props: route => Object.assign( route.query),
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_EMPLOYEE') {
                    next();
                  } else {
                    next({name: 'EmployeeLogin'});
                  }
                } else {
                  next({name: 'EmployeeLogin'});
                }
              },
            },
            {
              path: 'confirm-reservation',
              name: 'ConfirmReservation',
              component: EmployeeConfirmReservation,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_EMPLOYEE') {
                    next();
                  } else {
                    next({name: 'EmployeeLogin'});
                  }
                } else {
                  next({name: 'EmployeeLogin'});
                }
              },
            },
            {
              path: 'tickets',
              name: 'Tickets',
              component: EmployeeTickets,
              beforeEnter: (to, from, next) => {
                if (localStorage.getItem('user') != null) {
                  if (loginService.getRole(localStorage.getItem('user')) === 'ROLE_EMPLOYEE') {
                    next();
                  } else {
                    next({name: 'EmployeeLogin'});
                  }
                } else {
                  next({name: 'EmployeeLogin'});
                }
              },
            },
          ]
        }
      ]
    },
    {
      path: '/404',
      name: '404',
      component: NotFound
    },
    {
      path: '*',
      name: 'NotFound',
      redirect: '404'
    }
  ]
})
