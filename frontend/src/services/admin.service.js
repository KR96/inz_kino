import {authHeader} from "../helper/auth-header";

export const adminService = {
  addOrUpdateCinema,
  addOrUpdateArmchair,
  addOrUpdateActor,
  addOrUpdateDirector,
  addOrUpdateEmployee,
  addOrUpdateMovie,
  addOrUpdateProjection,
  addOrUpdateRoom,
  addOrUpdateShow,
  addOrUpdateStaff,
  addOrUpdateTicketType,
  addOrUpdateVersion
}

function addOrUpdateCinema(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateCinema`, requestOptions).then(handleResponse);
}

function addOrUpdateArmchair(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateArmchair`, requestOptions).then(handleResponse);
}

function addOrUpdateActor(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateActor`, requestOptions).then(handleResponse);
}

function addOrUpdateDirector(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateDirector`, requestOptions).then(handleResponse);
}

function addOrUpdateEmployee(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateEmployee`, requestOptions).then(handleResponse);
}

function addOrUpdateMovie(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateMovie`, requestOptions).then(handleResponse);
}

function addOrUpdateProjection(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateProjection`, requestOptions).then(handleResponse);
}

function addOrUpdateRoom(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateRoom`, requestOptions).then(handleResponse);
}

function addOrUpdateShow(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateShow`, requestOptions).then(handleResponse);
}

function addOrUpdateStaff(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateStaff`, requestOptions).then(handleResponse);
}

function addOrUpdateTicketType(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateTicketType`, requestOptions).then(handleResponse);
}

function addOrUpdateVersion(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/addOrUpdateVersion`, requestOptions).then(handleResponse);
}


function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        // logout();
        // location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }
    return data;
  });
}
