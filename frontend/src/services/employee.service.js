import {authHeader} from "../helper/auth-header";

export const employeeService = {
  confirmReservation,
  getReservationByCode,
  sellTicket,
  getCinemaByEmployeeLogin,
  getEmployeeByLogin,
  handleResponse
}

function sellTicket(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/employee/sellTicket`, requestOptions).then(handleResponse);
}

function confirmReservation(value) {
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json'},
    body: JSON.stringify(value)
  };

  return fetch(`/api/employee/confirmReservation`, requestOptions).then(handleResponse);
}

function getReservationByCode(value) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`/api/employee/getReservationByCode?confirmatoryCode=${value}`, requestOptions)
    .then(handleResponse)
    .then(response => {
      return response.data
    });
}

function getCinemaByEmployeeLogin(value) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`/api/employee/getCinemaByEmployeeLogin?login=${value}`, requestOptions)
    .then(handleResponse)
    .then(value => {
      if (value) {
        localStorage.setItem('employeesCinema', JSON.stringify(value));
      }
    });
}

function getEmployeeByLogin(value) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`/api/loggedEmployee/getEmployeeByLogin?login=${value}`, requestOptions)
    .then(handleResponse)
    .then(response => {
      return response;
    });
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        // logout();
        // location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }
    return data;
  });
}

