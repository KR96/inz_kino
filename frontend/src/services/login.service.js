import {authHeader} from "../helper/auth-header";
import {employeeService} from "./employee.service";

export const loginService = {
  login,
  register,
  logout,
  getRole,
  getEmail,
  getLogin,
  updateUserPassword,
  updateEmployeePassword,
  getUserByLogin,
  getReservationByUserLogin
};

function login(login, password) {
  const requestOptions = {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({login, password})
  };

  return fetch(`/api/login`, requestOptions)
    .then(handleResponse)
    .then(user => {
      // login successful if there's a jwt token in the response
      if (user) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('user', JSON.stringify(user));
      }

      return user;
    });
}

function register(user) {
  const requestOptions = {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(user)
  };

  return fetch(`/api/user/addOrUpdateUser`, requestOptions).then(handleResponse);
}

function logout() {
  localStorage.removeItem('user');
  localStorage.removeItem('employeesCinema');
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }
    return response.headers.get('authorization');
  })
}

function updateUserPassword(login, password, newpass) {
  const requestOptions = {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({login, password})
  };

  return fetch(`/api/login`, requestOptions)
    .then(handleResponse)
    .then(user => {
      if (user) {
        const requestOptionsPass = {
          method: 'POST',
          headers: {...authHeader(), 'Content-Type': 'application/json'},
          body: JSON.stringify(login, newpass)
        };
        return fetch(`/api/loggedUser/updateUserPassword?login=${login}&password=${newpass}`, requestOptionsPass).then(handleResponse);
      }
    });
}

function updateEmployeePassword(login, password, newpass) {
  const requestOptions = {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({login, password})
  };

  return fetch(`/api/login`, requestOptions)
    .then(handleResponse)
    .then(user => {
      if (user) {
        const requestOptionsPass = {
          method: 'POST',
          headers: {...authHeader(), 'Content-Type': 'application/json'},
          body: JSON.stringify(login, newpass)
        };
        return fetch(`/api/loggedEmployee/updateEmployeePassword?login=${login}&password=${newpass}`, requestOptionsPass).then(handleResponse);
      }
    });
}

function getUserByLogin(value) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`/api/loggedUser/getUserByLogin?login=${value}`, requestOptions)
    .then(employeeService.handleResponse)
    .then(response => {
      return response
    })
}

function getReservationByUserLogin(value) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`/api/loggedUser/getReservationByUser?login=${value}`, requestOptions)
    .then(employeeService.handleResponse)
    .then(response => {
      return response;
    })
}

function getRole(value) {
  let token = value.substring(7);
  let jwtData = token.split('.')[1];
  let decodedJwtJsonData = window.atob(jwtData);
  let decodedJwtData = JSON.parse(decodedJwtJsonData);

  return decodedJwtData.authorities[0];
}

function getEmail(value) {
  let token = value.substring(7);
  let jwtData = token.split('.')[1];
  let decodedJwtJsonData = window.atob(jwtData);
  let decodedJwtData = JSON.parse(decodedJwtJsonData);

  return decodedJwtData.email;
}

function getLogin(value) {
  let token = value.substring(7)
  let jwtData = token.split('.')[1]
  let decodedJwtJsonData = window.atob(jwtData)
  let decodedJwtData = JSON.parse(decodedJwtJsonData)

  return decodedJwtData.sub;
}
