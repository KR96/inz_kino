import Vue from 'vue';
import Vuex from 'vuex';

import { alert } from './alert.module';
import { store } from './store'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    alert,
    store
  }
});
