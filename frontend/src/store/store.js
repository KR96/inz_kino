import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const cinema = localStorage.getItem('cinema');
const logged = localStorage.getItem('user');
const userData = {};

const state = {
  cinema,
  logged,
  userData,
};

const getters = {
  getCinema: state => {
    return state.cinema;
  },
  isCinemaSet(state) {
    return state.cinema;
  },
  isLogged(state) {
    return state.logged;
  },
  getUserData(state) {
    return state.userData;
  }
};

const mutations = {
  setCinema(state, payload) {
    localStorage.setItem('cinema', payload);
    state.cinema = payload;
  },
  removeCinema(state) {
    localStorage.removeItem('cinema');
    state.cinema = null;
  },
  login(state, payload) {
    state.logged = payload;
  }
  ,
  storeLogout(state, payload) {
    localStorage.removeItem('user');
    state.logged = payload;
  },
  setUserData(state, payload) {
    state.userData = payload;
  },
  removeUserData(state, payload) {
    state.userData = {};
  },

};

const actions = {
  removeCinema(context) {
    context.commit('removeCinema');
  },
  setCinema(context, payload) {
    context.commit('setCinema', payload);
  },
  storeLogout(context) {
    context.commit('storeLogout');
  },
  login(context, payload) {
    context.commit('login', payload);
  },
  setUserData(context, payload) {
    context.commit('setUserData', payload);
  },
  removeUserData(context) {
    context.commit('removeUserData')
  }
};


export const store = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
