export default {
  methods: {
    checkReservationTime(timeValue) {
      let currTime = new Date()
      let timeValueObject = new Date(new Date(timeValue) - 20 * 60000);
      return timeValueObject < currTime;
    }
  }
}
