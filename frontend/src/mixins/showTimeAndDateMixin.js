export default {
  filters: {
    showTime(value) {
      if (!value) return '';
      return new Date(value).toLocaleTimeString("pl-PL", {hour: '2-digit', minute: '2-digit'});

    },
    showDate(value) {
      if (!value) return '';
      return new Date(value).toLocaleDateString("pl-PL", {
        weekday: 'long',
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
      });
    },
  }
}
