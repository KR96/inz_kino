export default {
  prepareWeekCalendar: function () {
    const offset = 7;
    const months = ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"];
    const weekdays = ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"];
    let result = [];
    let date = new Date();
    for (let i = 0; i < offset; i++) {
      let newDate = new Date(date.getTime() + (i * 24 * 60 * 60 * 1000));
      result.push({
        id: i,
        day: newDate.getDate(),
        month: newDate.getMonth() + 1,
        year: newDate.getFullYear(),
        weekday: weekdays[newDate.getDay()],
        monthName: months[newDate.getMonth()],
        dateVariable: newDate
      });
    }
    return result;
  }
}
