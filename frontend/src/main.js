import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import VueResources from "vue-resource"
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/style.css';

import store from './store/index';

Vue.config.productionTip = true;
Vue.use(BootstrapVue);
Vue.use(VueResources);


new Vue({
  el: '#app',
  store: store,
  router,
  components: {App},
  template: '<App/>'
});
